#include <stddef.h>
#include <string.h>
#include <stdlib.h>

void* zalloc(size_t sz)
{
	void* p = malloc(sz);
	if (!p) {
		return NULL;
	}
	memset(p, 0, sz);
	return p;		
}
