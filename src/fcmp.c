#include <stdint.h>
#include <stdio.h>

int fcmp(void* a, void* b, size_t maxlen) {
	size_t c;
	size_t x;
	size_t y;
	if (!a || !b) {
		return 1;
	}
	c = 0;
	x = maxlen / 8;
	y = (x + maxlen % 8);       
	while (c <= x) {
		if (((uint64_t*)a)[c] != ((uint64_t*)b)[c]) {
			return 1;
		}
		c++;
	}
	while (c <= y) {
		if (((uint8_t*)a)[c] != ((uint8_t*)b)[c]) {
			return 1;
		}
		c++;
	}
	return 0;
}
