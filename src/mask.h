/* Return mask for `_up' amount of upper bits from a `_bits' wide value */
#define UMASK(_up, _bits) (((1 << _up) - 1) << (_bits - _up))
/* Return mask for `_low' amount of lower bits from any width value */
#define LMASK(_low) ((1 << _low) - 1)
/* Return mask for `bits` amount of bits starting at `start' bits */
#define BMASK(_start, _size) (((1UL << _size) - 1) << _start)
